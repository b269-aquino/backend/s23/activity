// Real World Application Of Objects
/*
    - Scenario
        1. We would like to create a game that would have several pokemon interact with each other
        2. Every pokemon would have the same set of stats, properties and functions
*/

let trainer = {
    name: "Ash Ketchum",
    age: 10,
    friends: {
        hoen: ["May", "Max"],
        kanto: ["Brock", "Misty"]
    },
    pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
    talk: function(){
        console.log("Pikachu! I choose you!")
    }
}
console.log(trainer);
console.log("Resule of dot notation:");
console.log(trainer.name);
console.log("Result of square bracket notation");
console.log(trainer["pokemon"]);
trainer["pokemon"];
console.log("Resule of Talk method: ");
trainer.talk();

function Pokemon(name, level) {

    // Properties
    this.name = name;
    this.level = level;
    this.health = 2 * level;
    this.attack = level;

    //Methods
    this.tackle = function(target) {
        console.log(this.name + ' tackled ' + target.name);
        target.health = target.health - this.attack;
        console.log("targetPokemon's health is now reduced to " + target.health);
        if(target.health <= 1){
            console.log(target.name + ' fainted.')
        }
}
}

// Creates new instances of the "Pokemon" object each with their unique properties
let pikachu = new Pokemon("Pikachu", 50);
let geodude = new Pokemon("Geodude", 42);
let mewtwo = new Pokemon("Mewtwo", 36);
let rattata = new Pokemon('Rattata', 10);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

// Providing the "rattata" object as an argument to the "pikachu" tackle method will create interaction between the two objects
// mewtwo.tackle(pikachu);